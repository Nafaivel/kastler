# Kastler - markdown note manager with support of zettelkasten note taking system

<!-- ![License](https://img.shields.io/gitlab/license/Nafaivel/kastler?color=aec6cf&style=flat-square) -->
<!-- ![Lines of code](https://img.shields.io/tokei/lines/gitlab/Nafaivel/kastler?color=%232c94b0&style=flat-square) -->
<!-- ![GitLab last commit](https://img.shields.io/gitlab/last-commit/Nafaivel/kastler?color=%2377dd77&style=flat-square) -->

This project is an note manger, what aims to provide lightweight tui application
with functional of obsidian and apps like that with way of interacting with app like in vim,ranger,lf.
Kastler do not provide way to open inside it notes and preview it.
Notes will be opened by external editor and viewer, in my case emacs/nvim.

## Installation guide
For linux(and other unix-like operation systems):

1. install rust and cargo
2. `git clone https://gitlab.com/Nafaivel/kastler.git`
3. `cd ./kastler`
4. `cargo build --release`
5. add to path(options):
    * add this line to your shell rc in my case ~/.zshrc 
    `PATH=$PATH:path/to/kastler/target/release`
    * cp binary to `/bin/` or to `~/.local/bin/`
6. (optional (but in fact not because not will not work if notes path not `~/Documents/notes`(not creates by app)))
Create config (template you can find in ./config.yml) and copy in one of this places:
    * `$XDG_CONFIG_HOME/kastler`
    * `~/.config/kastler`
    * `~/.kastler`

## Way of work
If shortly you can just hit '?' to see all list of keymaps in current mode. (not yet imlemented)

Kastler now have N mods:
1. Main
2. Editing (without vim keybindings)
3. Tags
4. Notebooks
5. Confirm

### 1. Main
Main mode shows all notes what have user.

### 2. Editing
Usually shows when something is creating or renaming(renaming not yet implemented).

### 3. Tags
On this tab shows all tags and notes, which contains it.

### 4. Notebooks
Here user can create separated notebook(folder with notes)

### 5. Confirm
Usually shows when user tries to delete something

## Configuration

```
path2notes - path to notes
date_format - date format
editor - your editor or editor selected by xdg-open
brackets_around_link_is_enabled - display type of links [[this]] if true or this if false
exit_after_changing_note - exit from kastler after editing note
open_note_after_creating - after creating new note opens editor
langmaps - converts your another keyboard layout to english (english shold be in cfg)
start_menu - Menu with app starts possible values is {Notes, Tags, Links, Help}
keybindings - not yet implamented
```

## TODO
- via comand
  - `git grep todo!` and `git grep TODO`
  - or `rg -e "TODO|todo\!"`
- TODO: write useful documentation for keybindings (can be function like
  register_key(mode, "key", action, Some(description)) which will displayed in
  help menu)
