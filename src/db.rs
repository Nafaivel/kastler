use std::{fs, path::PathBuf};

use crate::notes::*;
use crate::settings::Settings;
use crate::App;
use crate::Error;
use dirs;

pub fn get_db_path() -> PathBuf {
    let mut local = PathBuf::new();
    local.push(dirs::data_local_dir().unwrap());
    local.push("kastler");
    if !local.exists() {
        fs::create_dir(&local).unwrap();
    }
    local.push("db.json");
    if !local.exists() {
        fs::write(&local, "[]").unwrap();
    }
    local
}

pub fn read_db() -> Result<Vec<Note>, Error> {
    let db_content = fs::read_to_string(get_db_path())?;
    let parsed: Vec<Note> = serde_json::from_str(&db_content)?;
    Ok(parsed)
}

pub fn read_db_parse_tags() -> Result<Vec<String>, Error> {
    let db_content = fs::read_to_string(get_db_path())?;
    let parsed: Vec<Note> = serde_json::from_str(&db_content)?;
    let mut tags: Vec<String> = vec![];
    for note in parsed {
        for tag in note.get_tags() {
            if !tags.contains(&tag) {
                tags.push(tag);
            }
        }
    }
    Ok(tags)
}

pub fn read_db_parse_notes_by_tag(tag: &String) -> Result<Vec<Note>, Error> {
    let mut notes: Vec<Note> = vec![];
    for note in read_db().unwrap() {
        if note.get_tags().contains(tag) {
            notes.push(note);
        }
    }
    Ok(notes)
}

pub fn init_db(settings: &Settings) {
    let path_to_notes = settings.get_notes_dir();
    let mut parsed: Vec<Note> = vec![];
    let paths = fs::read_dir(path_to_notes).unwrap();

    for path in paths {
        let path = path.unwrap().path();
        let filename = path.file_name().unwrap().to_str().unwrap();
        if filename.find(".md").is_some() {
            let note = Note::new_from_file(settings, &path);
            parsed.push(note);
            fs::write(
                get_db_path(),
                &serde_json::to_vec(&parsed).expect("faile to convert notes vec to json"),
            )
            .expect("failed to write notes to file while updating DB");
        }
    }
}

pub fn update_db(app: &App) {
    let mut path_to_notes = app.settings.get_notes_dir();
    if app.notebooks_state.current.selected().unwrap() != 0 {
        path_to_notes
            .push(&app.notebooks_state.notebooks[app.notebooks_state.current.selected().unwrap()]);
    }
    let mut parsed: Vec<Note> = vec![];
    let paths = fs::read_dir(path_to_notes).unwrap();

    for path in paths {
        let path = path.unwrap().path();
        let filename = path.file_name().unwrap().to_str().unwrap();
        if filename.find(".md").is_some() {
            let note = Note::new_from_file(&app.settings, &path);
            parsed.push(note);
        }
    }
    fs::write(
        get_db_path(),
        &serde_json::to_vec(&parsed).expect("faile to convert notes vec to json"),
    )
    .expect("failed to write notes to file while updating DB");
}
