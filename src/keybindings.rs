use crossterm::{
    event::{KeyCode, KeyEvent},
    execute,
    terminal::{disable_raw_mode, LeaveAlternateScreen},
};
use serde::{Deserialize, Serialize};
use std::io;
use std::sync::mpsc::Receiver;
use std::{fs, path::PathBuf};
use tui::{backend::CrosstermBackend, Terminal};
use unicode_segmentation::UnicodeSegmentation;

use crate::{
    init_db, notes::Note, ui::ui, App, Confirm, CreationPlace, EditingIntent, EditingMode, Event,
    InputMode, MenuItem, SelectedLinkWidget, SelectedTagWidget,
};

#[derive(Debug, Serialize, Deserialize)]
pub struct Keybindings {
    // TODO: create hashmap with all Keybindings folowing example
    //function: [char1, char2]
}

impl Keybindings {
    pub fn new() -> Keybindings {
        Keybindings {}
    }
}

pub fn keybindings(rx: Receiver<Event<KeyEvent>>) -> Result<(), Box<dyn std::error::Error>> {
    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.clear()?;

    let mut app = App::default();
    init_db(&app.settings);
    app.notes_state.sort_by_date();

    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        if app.need_to_quit {
            disable_raw_mode()?;
            terminal.show_cursor()?;
            execute!(io::stdout(), LeaveAlternateScreen)?;
            break;
        }

        match rx.recv()? {
            Event::Input(event) => match app.input_mode {
                InputMode::Sort => match_sort_keys(&mut app, event.code),
                InputMode::Editing => match_editing_keys(&mut app, event.code),
                InputMode::Normal => {
                    if app.notebooks_state.is_open {
                        match_notebook_keys(&mut app, event.code);
                    } else {
                        match_normal_keys(&mut app, event.code)
                    }
                }
                InputMode::Confirm => match_confirm_keys(&mut app, event.code),
                InputMode::Wait => {}
                _ => panic!("input mode must be here"),
            },
            Event::Tick => {}
        }
    }
    Ok(())
}

// HACK: think it can be writed better
fn find_char_pos(s: &str, c: char) -> Option<usize> {
    let mut counter = 0;
    for (i, grapheme) in s.grapheme_indices(true) {
        if grapheme == c.to_string() {
            return Some(counter);
        }
        counter += 1;
    }
    None
}

/// converts non-english char to english with help of keymaps
fn match_key_char(app: &App, key: char) -> char {
    let langmaps = app.settings.get_langmaps();
    if langmaps["en"].contains(key) {
        return key;
    } else {
        for (_name, langmap) in &langmaps {
            if let Some(pos) = find_char_pos(langmap, key) {
                return langmaps["en"]
                    .chars()
                    .nth(pos)
                    .expect("there always be element if langmap correct");
            }
        }
        return ' ';
    }
}

/// all sort keybindings
fn match_sort_keys(app: &mut App, code: KeyCode) {
    match code {
        KeyCode::Char(chr) => match match_key_char(app, chr) {
            'j' => {
                app.list_move(1);
            }
            'k' => {
                app.list_move(-1);
            }
            'q' => app.input_mode = InputMode::Normal,
            's' | 'l' => {
                app.input_mode = InputMode::Normal;
                app.sort_notes();
            }
            _ => {}
        },
        KeyCode::Esc => app.input_mode = InputMode::Normal,
        _ => {}
    }
}

/// all notebook keybindings
fn match_notebook_keys(app: &mut App, code: KeyCode) {
    match code {
        KeyCode::Char(chr) => match match_key_char(app, chr) {
            'q' | 'N' | 'l' => exit_from_notebook(app),
            'j' => app.list_move(1),
            'k' => app.list_move(-1),
            'a' => {
                app.editing_state.selected_index = 0;
                app.input_mode = InputMode::Editing;
                app.editing_state.intent = EditingIntent::NotebookName;
                app.notebooks_state.creation_place = CreationPlace::Near;
            }
            'A' => {
                app.editing_state.selected_index = 0;
                app.input_mode = InputMode::Editing;
                app.editing_state.intent = EditingIntent::NotebookName;
                app.notebooks_state.creation_place = CreationPlace::AtRoot;
            }
            'i' => {
                app.editing_state.selected_index = 0;
                app.input_mode = InputMode::Editing;
                app.editing_state.intent = EditingIntent::NotebookName;
                app.notebooks_state.creation_place = CreationPlace::Inside;
            }
            'd' => remove_notebook(app),
            _ => {}
        },
        KeyCode::Esc => exit_from_notebook(app),
        _ => {}
    }
}

/// removes notebook under cursor
fn remove_notebook(app: &mut App) {
    if fs::read_dir(app.notebooks_state.seleced_notebook_dir(&app.settings))
        .unwrap()
        .count()
        > 1
    {
        app.input_mode = InputMode::Confirm;
        app.confirm = Confirm::DeleteNotebook;
    } else {
        app.notebooks_state.remove_selected(&app.settings);
    }
}

/// exit from notebook when it's open and reloads app
fn exit_from_notebook(app: &mut App) {
    app.notebooks_state.is_open = false;
    app.reload();
    // update_db(&app);
}

/// all normal mode keybinding
fn match_normal_keys(app: &mut App, code: KeyCode) {
    match code {
        KeyCode::Esc => {
            if app.selected_menu != MenuItem::Notes {
                app.selected_menu = MenuItem::Notes
            }
        }
        KeyCode::Tab => {
            if app.selected_menu == MenuItem::Tags {
                match app.tag_list_state.selected {
                    SelectedTagWidget::Tags => {
                        app.tag_list_state.selected = SelectedTagWidget::Notes;
                    }
                    SelectedTagWidget::Notes => {
                        app.tag_list_state.selected = SelectedTagWidget::Tags;
                    }
                }
            } else if app.selected_menu == MenuItem::Links {
                match app.link_list_state.selected {
                    SelectedLinkWidget::Notes => {
                        app.link_list_state.selected = SelectedLinkWidget::Links
                    }
                    SelectedLinkWidget::Links => {
                        app.link_list_state.selected = SelectedLinkWidget::Notes
                    }
                }
            }
        }
        KeyCode::Char(chr) => match match_key_char(app, chr) {
            'q' => {
                app.need_to_quit = true;
            }
            '?' => app.selected_menu = MenuItem::Help,
            't' => app.selected_menu = MenuItem::Tags,
            'L' => {
                if !app.selected_menu.is_links() {
                    app.selected_menu = MenuItem::Links;
                } else if app.selected_menu.is_links() && app.link_list_state.selected.is_links() {
                    // todo!("set new note entry");
                    if app.link_list_state.selected.is_links() {
                        let link = app.link_list_state.selected_link(app);
                        if link.is_some() {
                            Note::create_from_filename(&app.settings, &link.unwrap().get_path());
                            // app.link_list_state.history.push(link.unwrap().get_path());
                            app.history_select();
                            app.link_list_state.selected = SelectedLinkWidget::Notes;
                        }
                    }
                } else {
                    app.link_list_state.selected = SelectedLinkWidget::Links
                }
            }
            'H' => {
                if app.selected_menu.is_links() && app.link_list_state.selected.is_notes() {
                    app.history_prev();
                } else {
                    app.link_list_state.selected = SelectedLinkWidget::Notes
                }
            }
            'n' => app.selected_menu = MenuItem::Notes,
            'N' => app.notebooks_state.is_open = true,
            // TODO: add new keybinding to yank filename
            'o' => {
                // create new note
                if app.selected_menu.is_notes()
                    | app.selected_menu.is_links()
                    | app.selected_menu.is_tags()
                {
                    // Note::add_random_to_db().expect("can't add new random note");
                    app.editing_state.selected_index = 0;
                    app.input_mode = InputMode::Editing;
                    app.editing_state.intent = EditingIntent::NoteName;
                }
            }
            'e' => {
                // open note
                app.edit_at_index().unwrap();
            }
            'a' => {
                // rename
                // TODO: improve renaming by renaming also links to renamed note
                if app.selected_menu.is_notes() {
                    if app.notes_state.notes.len() != 0 {
                        app.input_mode = InputMode::Editing;
                        app.editing_state.intent = EditingIntent::ChangeNoteName;
                        let path = PathBuf::from(app.notes_state.selected_note().get_path())
                            .file_name()
                            .unwrap()
                            .to_str()
                            .unwrap()
                            .trim_end_matches(".md")
                            .trim_start_matches("/")
                            .to_string();
                        app.editing_state.input = path;
                        app.editing_state.selected_index = app.editing_state.input.chars().count();
                    }
                } else if app.selected_menu.is_tags() {
                    // TODO: rename only tag even if selected notes menu
                    app.input_mode = InputMode::Editing;
                    app.editing_state.intent = EditingIntent::ChangeTagName;
                    app.editing_state.input =
                        app.get_all_tags()[app.tag_list_state.tags.selected().unwrap()].clone();
                    // .trim_start_matches("#")
                    // .to_string();
                }
                // TODO: add renaming inside link menu
            }
            's' => {
                app.input_mode = InputMode::Sort;
            }
            'l' => {
                // edit if Notes or Tags-Notes selected
                if app.selected_menu == MenuItem::Notes
                    || (app.selected_menu == MenuItem::Tags
                        && app.tag_list_state.selected == SelectedTagWidget::Notes)
                {
                    app.edit_at_index().unwrap();
                    if app.settings.exit_after_changing_note_is_enabled() {
                        app.need_to_quit = true;
                    }
                // if link selected
                } else if app.selected_menu.is_links()
                    && app.link_list_state.selected == SelectedLinkWidget::Links
                {
                    app.edit_at_index().unwrap();
                // from Tags-Tags to Tags-Notes
                } else if app.selected_menu == MenuItem::Tags
                    && app.tag_list_state.selected == SelectedTagWidget::Tags
                {
                    app.tag_list_state.selected = SelectedTagWidget::Notes;
                // from Links-Notes to Links-Links
                } else if app.selected_menu == MenuItem::Links
                    && app.link_list_state.selected == SelectedLinkWidget::Notes
                {
                    app.link_list_state.selected = SelectedLinkWidget::Links;
                }
            }
            'h' => {
                if app.selected_menu == MenuItem::Links
                    && app.link_list_state.selected == SelectedLinkWidget::Links
                {
                    app.link_list_state.selected = SelectedLinkWidget::Notes;
                } else if app.selected_menu == MenuItem::Tags
                    && app.tag_list_state.selected == SelectedTagWidget::Notes
                {
                    app.tag_list_state.selected = SelectedTagWidget::Tags;
                }
            }
            'd' => {
                if app.selected_menu == MenuItem::Notes {
                    app.input_mode = InputMode::Confirm;
                    app.confirm = Confirm::DeleteNote;
                }
            }
            'u' => {
                app.update_notes();
                app.sort_notes();
            }
            // TODO: 'G', 'g' support to all menus
            'g' => app.list_move_top(),
            'G' => app.list_move_bottom(),
            'j' => app.list_move(1),
            'k' => app.list_move(-1),
            'J' => app.list_move(5),
            'K' => app.list_move(-5),
            _ => {}
        },
        _ => {}
    }
}

// TODO: add vim like movement in editing
// NOTE: watch app.editing_state.selected_index this is cursor position
/// all editing keybindings
fn match_editing_keys(app: &mut App, code: KeyCode) {
    match app.editing_state.mode {
        EditingMode::Insert => _insert_mode_bindings(app, code),
        EditingMode::Normal => _normal_mode_bindings(app, code),
    }
}

fn _normal_mode_bindings(app: &mut App, code: KeyCode) {
    match code {
        KeyCode::Enter => editing_enter(app),
        KeyCode::Esc => {
            app.editing_state.input.drain(..);
            app.input_mode = InputMode::Normal;
            app.editing_state.intent = EditingIntent::None;
            app.editing_state.mode = EditingMode::Insert;
        }
        KeyCode::Left => app.editing_state.move_pos(-1),
        KeyCode::Right => app.editing_state.move_pos(1),
        KeyCode::Char(chr) => match chr {
            'i' => app.editing_state.mode = EditingMode::Insert,
            'a' => {
                app.editing_state.mode = EditingMode::Insert;
                app.editing_state.move_pos(1);
            }
            'h' => app.editing_state.move_pos(-1),
            'l' => app.editing_state.move_pos(1),
            'b' => app.editing_state.move_pos_word(-1),
            'w' => app.editing_state.move_pos_word(1),

            _ => {}
        },
        _ => {}
    }
}

fn _insert_mode_bindings(app: &mut App, code: KeyCode) {
    match code {
        KeyCode::Enter => editing_enter(app),
        KeyCode::Left => app.editing_state.move_pos(-1),
        KeyCode::Right => app.editing_state.move_pos(1),
        KeyCode::Backspace => app.editing_state.del(-1),
        KeyCode::Esc => {
            app.editing_state.mode = EditingMode::Normal;
            app.editing_state.move_pos(-1);
        }
        KeyCode::Char(chr) => {
            if chr == ' ' {
                app.editing_state.insert('_');
            } else {
                app.editing_state.insert(chr);
            }
        }
        _ => {}
    }
}

fn editing_enter(app: &mut App) {
    let input: String = app.editing_state.input.drain(..).collect();
    match app.editing_state.intent {
        EditingIntent::NotebookName => {
            // place of creation notebook
            if app.notebooks_state.creation_place == CreationPlace::AtRoot {
                app.notebooks_state
                    .create_in_notes_dir(&app.settings, input);
            } else if app.notebooks_state.creation_place == CreationPlace::Inside {
                app.notebooks_state.create_inside_dir(&app.settings, input);
            } else if app.notebooks_state.creation_place == CreationPlace::Near {
                app.notebooks_state.create_in_same_dir(&app.settings, input);
            }
            app.notebooks_state.creation_place = CreationPlace::None;
            app.update_notebooks();
            app.notebooks_state.is_open = true;
        }
        EditingIntent::NoteName => {
            Note::add_to_db(app, input.replace(" ", "_")).expect("failed_to_add");
            if app.settings.open_note_after_creating_is_enabled() {
                app.note_edit_last().unwrap();
            }
        }
        EditingIntent::ChangeNoteName => {
            Note::rename(app, input.replace(" ", "_") + ".md");
        }
        EditingIntent::ChangeTagName => {
            let old_tag_name =
                app.get_all_tags()[app.tag_list_state.tags.selected().unwrap()].clone();
            let new_tag_name: String = input;
            app.rename_tag(old_tag_name, dbg!(new_tag_name));
            // println!("renamed");
        }
        EditingIntent::None => {}
        _ => panic!("not existing editing mode"),
    }
    app.input_mode = InputMode::Normal;
    app.editing_state.intent = EditingIntent::None;
}

/// all confirm keys
fn match_confirm_keys(app: &mut App, code: KeyCode) {
    match code {
        KeyCode::Char(chr) => match match_key_char(app, chr) {
            'y' => {
                if app.confirm == Confirm::DeleteNote {
                    app.remove_at_index().unwrap();
                } else if app.confirm == Confirm::DeleteNotebook {
                    app.notebooks_state.remove_selected(&app.settings);
                }
            }
            'n' | 'q' => {
                app.input_mode = InputMode::Normal;
                app.confirm = Confirm::None;
            }
            _ => {}
        },
        KeyCode::Esc => {
            app.input_mode = InputMode::Normal;
            app.confirm = Confirm::None;
        }
        _ => {}
    }
}
