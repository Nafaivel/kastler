#![allow(unreachable_patterns)]
#![allow(dead_code)]

use crossterm::{
    event::{self, Event as CEvent},
    execute,
    terminal::{enable_raw_mode, EnterAlternateScreen},
};
use db::init_db;
use libc::{fork, setsid};
use notes::Note;
use serde::{Deserialize, Serialize};
use settings::Settings;
use std::os::unix::process::CommandExt;
use std::process::Command;
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, Instant};
use std::{fs, vec};
use std::{io, path::PathBuf};
use thiserror::Error;
use tui::widgets::ListState;

use unicode_segmentation::UnicodeSegmentation;
mod db;
mod keybindings;
mod notes;
mod settings;
mod tests;
mod ui;

use keybindings::keybindings;

#[derive(Error, Debug)]
pub enum Error {
    #[error("error reading the DB file: {0}")]
    ReadDBError(#[from] io::Error),
    #[error("error parsing the DB file: {0}")]
    ParseDBError(#[from] serde_json::Error),
}

pub enum Event<I> {
    Input(I),
    Tick,
}

#[derive(Copy, Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum MenuItem {
    Notes,
    Help,
    Tags,
    Links,
}

impl MenuItem {
    pub fn is_links(&self) -> bool {
        self == &Self::Links
    }

    pub fn is_notes(&self) -> bool {
        self == &Self::Notes
    }

    pub fn is_help(&self) -> bool {
        self == &Self::Help
    }

    pub fn is_tags(&self) -> bool {
        self == &Self::Tags
    }
}

#[derive(PartialEq)]
enum InputMode {
    Normal,
    Editing,
    Wait,
    Confirm,
    Sort,
}

#[derive(PartialEq)]
enum Confirm {
    DeleteNote,
    DeleteNotebook,
    None,
}

#[derive(PartialEq)]
enum EditingIntent {
    NoteName,
    ChangeNoteName,
    ChangeTagName,
    NotebookName,
    None,
}

#[derive(PartialEq)]
enum SelectedTagWidget {
    Tags,
    Notes,
}

impl SelectedTagWidget {
    fn is_tags(&self) -> bool {
        self == &SelectedTagWidget::Tags
    }
    fn is_notes(&self) -> bool {
        self == &SelectedTagWidget::Notes
    }
}

#[derive(PartialEq)]
enum SelectedLinkWidget {
    Links,
    Notes,
}

impl SelectedLinkWidget {
    fn is_links(&self) -> bool {
        self == &SelectedLinkWidget::Links
    }

    fn is_notes(&self) -> bool {
        self == &SelectedLinkWidget::Notes
    }
}

#[derive(PartialEq, Clone, Copy)]
pub enum SortType {
    ByDate,
    ByDateRev,
    ByName,
    ByNameRev,
}

impl From<MenuItem> for usize {
    fn from(input: MenuItem) -> usize {
        match input {
            MenuItem::Links => 3,
            MenuItem::Tags => 2,
            MenuItem::Help => 1,
            MenuItem::Notes => 0,
            _ => panic!("menu item"),
        }
    }
}

pub struct HistEntry {
    notes: Vec<Note>,
    selected_note_prev_menu: usize,
    selected_link_prev_menu: usize,
}

impl HistEntry {
    fn new(notes: Vec<Note>, note_id: usize, link_id: usize) -> HistEntry {
        HistEntry {
            notes,
            selected_note_prev_menu: note_id,
            selected_link_prev_menu: link_id,
        }
    }

    /// returns HistEntry for LinkListState
    /// and contains notes parsed from links
    fn new_from_note(
        note_id: usize,
        link_id: usize,
        note: &Note,
        settings: &Settings,
    ) -> HistEntry {
        Self::new(
            note.get_note_refs()
                .into_iter()
                .map(|link| {
                    Note::create_from_filename(settings, &LinkListState::normalize_link(link))
                })
                .collect(),
            note_id,
            link_id,
        )
    }

    pub fn selected_link_prev_menu(&self) -> usize {
        self.selected_link_prev_menu
    }

    pub fn selected_note_prev_menu(&self) -> usize {
        self.selected_note_prev_menu
    }

    pub fn get_notes(&self) -> &Vec<Note> {
        &self.notes
    }
}

pub struct LinkListState {
    links: ListState,
    notes: ListState,
    selected: SelectedLinkWidget,
    /// contains filename (or note.path if look at implementation)
    history: Vec<HistEntry>,
}

impl LinkListState {
    fn new() -> LinkListState {
        let mut link_list_state = ListState::default();
        link_list_state.select(Some(0));
        let mut note_list_state = ListState::default();
        note_list_state.select(Some(0));
        LinkListState {
            links: link_list_state,
            notes: note_list_state,
            selected: SelectedLinkWidget::Notes,
            history: vec![],
        }
    }

    fn selected_note(&self, app: &App) -> Note {
        if self.history.is_empty() {
            app.notes_state.notes[app.link_list_state.notes.selected().unwrap()].clone()
        } else {
            self.history
                .last()
                .expect("last histentry")
                // .selected_note_prev_menu()
                .get_notes()
                .iter()
                .nth(self.notes.selected().expect("allways be selected"))
                .expect("selected note")
                .clone()
        }
    }

    pub fn normalize_link(link: String) -> String {
        link.trim_end()
            .trim_start()
            .trim_end()
            .trim_end_matches("]]")
            .trim_start_matches("[[")
            .trim_start_matches('.')
            .trim_start_matches('/')
            .to_string()
    }

    fn selected_note_normalized_links(&self, app: &App) -> Vec<String> {
        let selected_note = self.selected_note(app);
        let mut note_domain = self
            .selected_note(app)
            .get_path()
            .trim_start_matches('.')
            .trim_start_matches('/')
            .to_string();
        note_domain.truncate(match note_domain.rfind('/') {
            Some(val) => val + 1,
            None => 0,
        });

        let note_refs = selected_note.get_note_refs();
        let mut normalized_refs: Vec<String> = vec![];
        let non_root_domain = note_domain.contains('/');

        for refer in note_refs {
            if non_root_domain {
                normalized_refs.push(note_domain.clone() + &Self::normalize_link(refer));
            } else {
                normalized_refs.push(Self::normalize_link(refer));
            }
        }
        normalized_refs
    }

    /// returns None on no note\_refs inside
    /// otherwise returns note
    /// if note not exists creats it
    fn selected_link(&self, app: &App) -> Option<Note> {
        let note_refs = self.selected_note_normalized_links(app);

        if note_refs.is_empty() {
            return None;
        }

        let filename = note_refs[app.link_list_state.links.selected().unwrap()].clone();
        let note = Note::new_from_filename(&app.settings, &filename);

        if note.is_none() {
            Some(Note::create_from_filename(&app.settings, &filename))
        } else {
            note
        }
    }
}

pub struct TagListState {
    tags: ListState,
    notes: ListState,
    selected: SelectedTagWidget,
    sort_pos: ListState,
}

impl TagListState {
    fn new() -> TagListState {
        let mut list_state = ListState::default();
        list_state.select(Some(0));
        let mut spls = ListState::default();
        spls.select(Some(2));
        TagListState {
            tags: list_state.clone(),
            notes: list_state,
            sort_pos: spls,
            selected: SelectedTagWidget::Tags,
        }
    }

    pub fn selected_note(&self, app: &App) -> Note {
        let selected = app.tag_list_state.notes.selected().unwrap();
        let items = app
            .get_notes_by_tag(&app.get_all_tags()[app.tag_list_state.tags.selected().unwrap()])
            .unwrap();
        items[selected].clone()
    }
}

#[derive(PartialEq)]
pub enum CreationPlace {
    None,
    Inside,
    AtRoot,
    Near,
}

pub struct NotebooksState {
    notebooks: Vec<String>,
    current: ListState,
    is_open: bool,
    creation_place: CreationPlace,
}

impl NotebooksState {
    fn new(settings: &Settings) -> NotebooksState {
        let mut current = ListState::default();
        current.select(Some(0));
        NotebooksState {
            notebooks: Self::find_notebooks(settings),
            is_open: false,
            current,
            creation_place: CreationPlace::None,
        }
    }

    pub fn seleced_notebook(&self) -> String {
        self.notebooks[self.current.selected().unwrap()].clone()
    }

    pub fn seleced_notebook_dir(&self, settings: &Settings) -> PathBuf {
        let mut path = settings.get_notes_dir();
        if self.current.selected().unwrap() != 0 {
            path.push(&self.seleced_notebook())
        }
        path
    }

    pub fn create_in_same_dir(&self, settings: &Settings, name: String) {
        let mut path = settings.get_notes_dir();
        for p in self.seleced_notebook().split('/') {
            path.push(p)
        }
        path = PathBuf::from(path.parent().unwrap());
        path.push(name.split('/').next().unwrap());
        Self::create(&mut path);
    }

    pub fn create_inside_dir(&self, settings: &Settings, name: String) {
        let mut path = settings.get_notes_dir();
        for p in self.seleced_notebook().split('/') {
            path.push(p)
        }
        path.push(name.split('/').next().unwrap());
        Self::create(&mut path);
    }

    pub fn create_in_notes_dir(&self, settings: &Settings, name: String) {
        let mut path = settings.get_notes_dir();
        path.push(name.split('/').next().unwrap());
        Self::create(&mut path);
    }

    fn create(path: &mut PathBuf) {
        fs::create_dir_all(path.clone()).expect("failed to create dir");
        path.push(".nb");
        fs::write(path, "").expect("failed to create notebook file");
    }

    pub fn remove_selected(&mut self, settings: &Settings) {
        fs::remove_dir_all(self.seleced_notebook_dir(settings)).expect("cant remove dir");
        self.notebooks.remove(self.current.selected().unwrap());
    }

    fn find_notebooks(settings: &Settings) -> Vec<String> {
        let mut nb = vec!["Main".to_string()];
        Self::recursive_dir_check_for_md(settings, settings.get_notes_dir(), &mut nb);
        nb
    }

    fn recursive_dir_check_for_md(
        settings: &Settings,
        path: PathBuf,
        notebook_paths: &mut Vec<String>,
    ) -> Vec<String> {
        let paths = fs::read_dir(path.clone()).unwrap();
        for tpath in paths {
            let tpath = tpath.unwrap().path();
            if tpath.is_dir() {
                Self::recursive_dir_check_for_md(settings, tpath, notebook_paths);
            } else if tpath.to_str().unwrap().ends_with(".md")
                || tpath.file_name().unwrap().to_str().unwrap() == ".nb"
            {
                let push_path = path
                    .to_str()
                    .unwrap()
                    .to_string()
                    .trim_start_matches(settings.get_notes_dir().to_str().unwrap())
                    .to_string()
                    .trim_start_matches("/")
                    .to_string();
                if !notebook_paths.contains(&push_path) && push_path != "" {
                    notebook_paths.push(push_path);
                }
            }
        }
        return notebook_paths.clone();
    }

    pub fn update(&mut self, settings: &Settings) {
        self.notebooks = Self::find_notebooks(settings);
    }

    pub fn get_notebooks(&self) -> Vec<String> {
        self.notebooks.clone()
    }
}

struct NotesState {
    notes: Vec<Note>,
    pos: ListState,
    sort_pos: ListState,
}

impl NotesState {
    fn new() -> NotesState {
        let mut note_list_state = ListState::default();
        note_list_state.select(Some(0));
        let mut sort_state = ListState::default();
        sort_state.select(Some(0));
        NotesState {
            notes: db::read_db().unwrap(),
            pos: note_list_state,
            sort_pos: sort_state,
        }
    }
    fn update(&mut self) {
        self.notes = db::read_db().unwrap();
    }

    fn by_name(&mut self) -> Vec<Note> {
        let mut names = vec![];
        for note in self.notes.clone() {
            names.push(note.get_name());
        }
        names.sort();
        let mut new_notes: Vec<Note> = vec![];
        for name in names {
            for (i_note, note) in self.notes.clone().into_iter().enumerate() {
                if name == note.get_name() {
                    new_notes.push(note);
                    self.notes.remove(i_note);
                    break;
                }
            }
        }
        new_notes
    }

    fn by_date(&mut self) -> Vec<Note> {
        let mut dates = vec![];
        for note in self.notes.clone() {
            dates.push(note.get_date());
        }
        dates.sort();
        let mut new_notes: Vec<Note> = vec![];
        for date in dates {
            for (i_note, note) in self.notes.clone().into_iter().enumerate() {
                if date == note.get_date() {
                    new_notes.push(note);
                    self.notes.remove(i_note);
                    break;
                }
            }
        }
        new_notes
    }

    pub fn selected_note(&self) -> Note {
        self.notes[self.pos.selected().unwrap()].clone()
    }

    fn sort_by_name_rev(&mut self) {
        let mut notes = Self::by_name(self);
        notes.reverse();
        self.notes = notes;
    }

    fn sort_by_name(&mut self) {
        self.notes = Self::by_name(self);
    }

    fn sort_by_date_rev(&mut self) {
        let mut notes = Self::by_date(self);
        notes.reverse();
        self.notes = notes;
    }

    fn sort_by_date(&mut self) {
        self.notes = Self::by_date(self);
    }
}

pub enum EditingMode {
    Normal,
    Insert,
    // visual,
}

pub struct EditingState {
    intent: EditingIntent,
    mode: EditingMode,
    input: String,
    selected_index: usize,
}

impl EditingState {
    pub fn new() -> EditingState {
        EditingState {
            intent: EditingIntent::None,
            mode: EditingMode::Insert,
            input: "".to_string(),
            selected_index: 0,
        }
    }

    /// paste char after cursor pos (selected_index)
    pub fn insert(&mut self, chr: char) {
        if self.input.grapheme_indices(true).count() == self.selected_index {
            self.input.push(chr);
            self.move_pos(1);
        } else {
            let mut temp_str = String::new();
            for (i, character) in self
                .input
                .grapheme_indices(true)
                .map(|(_, s)| s)
                .enumerate()
            {
                if i == self.selected_index {
                    temp_str.push(chr);
                }
                temp_str.push_str(character);
            }
            self.move_pos(1);
            self.input = temp_str
        }
    }

    fn _filter(&self, i: usize, negative: bool, back: usize, skip: usize) -> bool {
        if negative {
            !((i < self.selected_index + back) && (i >= self.selected_index))
        } else {
            !((i >= self.selected_index) && (i < self.selected_index + skip))
        }
    }

    /// remove -n<--amount-->n chars before/after selected_index
    pub fn del(&mut self, amount: i8) {
        let mut back: usize = 0;
        let mut skip = amount.unsigned_abs() as usize;
        if amount.is_negative() {
            back = amount.unsigned_abs() as usize;
            skip = 0;
            if self.selected_index > amount.unsigned_abs() as usize {
                self.move_pos(amount)
            } else if self.selected_index == 0 {
                return;
            } else {
                // HACK: not work with amount > 1
                self.move_pos(amount);
                self.input = self
                    .input
                    .grapheme_indices(true)
                    .skip(1)
                    .map(|(_, s)| s)
                    .collect::<Vec<&str>>()
                    .join("");
                return;
            }
        }
        self.input = self
            .input
            .grapheme_indices(true)
            .map(|(_, s)| s)
            .collect::<Vec<&str>>()
            .iter()
            .enumerate()
            .filter(|&(i, _)| self._filter(i, amount.is_negative(), back, skip))
            .map(|(_, s)| *s)
            .collect::<Vec<&str>>()
            .join("");

        // self.input.remove(self.selected_index);
        // self.input.pop();
        // if !self.input.is_empty() {
        //     self.selected_index -= 1
        // } else {
        //     self.selected_index = 0
        // }
    }

    /// move -n<--amount-->n chars before/after selected_index
    pub fn move_pos(&mut self, amount: i8) {
        if amount < 0 {
            // left
            if -amount as usize > self.selected_index {
                self.selected_index = 0
            } else {
                self.selected_index -= -amount as usize;
            }
        } else {
            // right
            if self.selected_index >= self.input.grapheme_indices(true).count() {
            } else if amount as usize
                > self.input.grapheme_indices(true).count() + self.selected_index
            {
                self.selected_index = self.input.grapheme_indices(true).count()
            } else {
                self.selected_index += amount as usize
            }
        }
    }

    /// move -n<--amount-->n words before/after selected_index
    pub fn move_pos_word(&mut self, word_amount: i8) {
        let mut wa = word_amount;
        while wa != 0 {
            if word_amount.is_negative() {
                match self.input.graphemes(true).collect::<Vec<&str>>()[0..self.selected_index]
                    .iter()
                    .rev()
                    .position(|&r| r == " " || r == "_" || r == "-")
                {
                    Some(pos) => {
                        wa += 1;
                        self.move_pos(-(pos as i8 + 1));
                    }
                    None => {
                        wa = 0;
                        self.selected_index = 0
                    }
                }
            } else {
                let input_len = self.input.grapheme_indices(true).count();
                let plus_one_flag;
                let cur_pos = if self.selected_index >= input_len {
                    plus_one_flag = false;
                    self.selected_index
                } else {
                    plus_one_flag = true;
                    self.selected_index + 1
                };
                match self.input.graphemes(true).collect::<Vec<&str>>()[cur_pos..]
                    .iter()
                    .position(|&r| r == " " || r == "_" || r == "-")
                {
                    Some(pos) => {
                        if plus_one_flag {
                            self.move_pos(pos as i8 + 1)
                        } else {
                            self.move_pos(pos as i8)
                        }
                        wa -= 1;
                    }
                    None => {
                        wa = 0;
                        self.selected_index = input_len
                    }
                }
            }
        }
    }
}

pub struct App {
    tag_list_state: TagListState,
    link_list_state: LinkListState,
    notebooks_state: NotebooksState,
    notes_state: NotesState,
    editing_state: EditingState,
    sort_types: Vec<SortType>,
    selected_menu: MenuItem,
    input_mode: InputMode,
    // intent: EditingIntent,
    // input: String,
    confirm: Confirm,
    settings: Settings,
    need_to_quit: bool,
}

/// Struct, where stores all runtime data
impl App {
    /// Create default instance of App
    fn default() -> App {
        let settings = Settings::get_settings();
        let mut notes_state = NotesState::new();
        notes_state.sort_by_name();
        App {
            selected_menu: settings.get_start_menu(),
            tag_list_state: TagListState::new(),
            link_list_state: LinkListState::new(),
            notes_state,
            editing_state: EditingState::new(),
            input_mode: InputMode::Normal,
            // intent: EditingIntent::None,
            // input: String::new(),
            confirm: Confirm::None,
            notebooks_state: NotebooksState::new(&settings),
            settings,
            sort_types: vec![
                SortType::ByDate,
                SortType::ByDateRev,
                SortType::ByName,
                SortType::ByNameRev,
            ],
            need_to_quit: false,
        }
    }

    /// Move history forward
    fn history_select(&mut self) {
        let some_note = self.link_list_state.selected_link(self);
        if let Some(..) = some_note {
            self.link_list_state.history.push(HistEntry::new_from_note(
                self.link_list_state
                    .notes
                    .selected()
                    .expect("selected note"),
                self.link_list_state
                    .links
                    .selected()
                    .expect("selected link"),
                &some_note.unwrap(),
                &self.settings,
            ));
        }
        self.link_list_state.notes.select(Some(0));
        self.link_list_state.links.select(Some(0));
    }

    fn history_prev(&mut self) {
        if let Some(item) = self.link_list_state.history.pop() {
            self.link_list_state
                .notes
                .select(Some(item.selected_note_prev_menu()));
            self.link_list_state
                .links
                .select(Some(item.selected_link_prev_menu()));
            self.link_list_state.selected = SelectedLinkWidget::Links;
        }
    }

    /// Sort notes list by type stored in SortType
    fn sort_notes(&mut self) {
        match self.sort_types[self.notes_state.sort_pos.selected().unwrap()] {
            SortType::ByDate => self.notes_state.sort_by_date(),
            SortType::ByDateRev => self.notes_state.sort_by_date_rev(),
            SortType::ByName => self.notes_state.sort_by_name(),
            SortType::ByNameRev => self.notes_state.sort_by_name_rev(),
            _ => panic!("implement new sort type"),
        }
    }

    /// Renames tag inside all notes (changes file data) in App.notes_state.notes
    fn rename_tag(&mut self, old_name: String, new_name: String) {
        for note in &mut self.notes_state.notes {
            note.rename_tag(&self.settings, &old_name, &new_name);
        }
    }

    /// Quick access to reset notebook from app
    fn update_notebooks(&mut self) {
        self.notebooks_state = NotebooksState::new(&self.settings);
    }

    /// Get vec of all tags inside notes
    fn get_all_tags(&self) -> Vec<String> {
        let notes: Vec<Note> = self.notes_state.notes.clone();
        let mut tags: Vec<String> = vec![];
        for note in notes {
            for tag in note.get_tags() {
                if !tags.contains(&tag) {
                    tags.push(tag);
                }
            }
        }
        // SortType support
        match self.sort_types[self.tag_list_state.sort_pos.selected().unwrap()] {
            SortType::ByName => tags.sort(),
            SortType::ByNameRev => {
                tags.sort();
                tags.reverse();
            }
            _ => {}
        }
        tags
    }

    /// Get notes contain vec name
    fn get_notes_by_tag(&self, tag: &String) -> Option<Vec<Note>> {
        let mut notes: Vec<Note> = vec![];
        for note in self.notes_state.notes.clone() {
            if note.get_tags().contains(tag) {
                notes.push(note);
            }
        }
        if notes.is_empty() {
            return None;
        }
        Some(notes)
    }

    /// edit selected note (under the cursor)
    pub fn edit_at_index(&self) -> Result<(), Error> {
        let note: Note;
        if self.selected_menu.is_notes() {
            note = self.notes_state.selected_note();
        } else if self.selected_menu.is_tags()
            && self.tag_list_state.selected == SelectedTagWidget::Notes
        {
            note = self.tag_list_state.selected_note(self);
        } else if self.selected_menu.is_links() && self.link_list_state.selected.is_notes() {
            note = self.link_list_state.selected_note(self);
        } else if self.selected_menu.is_links() && self.link_list_state.selected.is_links() {
            let some_note = self.link_list_state.selected_link(self);
            if some_note.is_none() {
                // return Ok(());
                note = self.link_list_state.selected_note(self);
            } else {
                note = some_note.unwrap()
            }
        } else {
            return Ok(());
        }
        let mut path2note = self.settings.get_notes_dir();
        path2note.push(note.get_path());
        self.note_edit_by_path(path2note);
        Ok(())
    }

    ///  edit last ceated note used in edit after creation
    pub fn note_edit_last(&self) -> Result<(), Error> {
        let mut path2note = self.settings.get_notes_dir();
        path2note.push(self.notes_state.notes.last().unwrap().get_path());
        self.note_edit_by_path(path2note);
        Ok(())
    }

    /// edit note by privided path
    pub fn note_edit_by_path(&self, path2note: PathBuf) {
        let editor = self.settings.get_editor();
        let editor_args = self.settings.get_editor_args();

        let path = path2note.to_str().unwrap().to_string();

        if !path2note.exists() {
            fs::File::create(path2note).expect("create failed");
        }

        // selecting editor from settings or xdg-open in not defined in settings
        unsafe {
            let mut editor = Command::new(editor)
                .args(editor_args)
                .arg(path)
                .stdin(std::process::Stdio::null())
                .stdout(std::process::Stdio::null())
                .stderr(std::process::Stdio::piped())
                .pre_exec(|| {
                    // Detach the child process from the current session
                    if fork() > 0 {
                        std::process::exit(0);
                    }
                    setsid();
                    Ok(())
                })
                .spawn()
                .unwrap();

            // Close the parent's copies of the child's file descriptors
            drop(editor.stdout.take());
            drop(editor.stderr.take());
        }
    }

    /// remove selected note (under the cursor)
    pub fn remove_at_index(&mut self) -> Result<(), Error> {
        let selected = self.notes_state.pos.selected().unwrap();
        if !self.notes_state.notes.is_empty() {
            let mut note_path = self.settings.get_notes_dir();
            note_path.push(self.notes_state.notes[selected].get_path());
            if note_path.exists() {
                fs::remove_file(note_path).unwrap();
            }
            self.notes_state.notes.remove(selected);
        }
        fs::write(
            db::get_db_path(),
            &serde_json::to_vec(&self.notes_state.notes)?,
        )?;
        if selected == 0 {
            self.notes_state.pos.select(Some(selected));
        } else {
            self.notes_state.pos.select(Some(selected - 1));
        }
        Ok(())
    }

    /// rescan notes dir and select 0 in notes
    fn update_notes(&mut self) {
        db::update_db(&self);
        self.notes_state.update();
        self.notes_state.pos.select(Some(0));
        // self.notes_state.sort_by_date_rev();
    }

    /// set default states of notes, tags, links calls on 'u'
    fn reload(&mut self) {
        self.update_notes();
        self.tag_list_state = TagListState::new();
        self.link_list_state = LinkListState::new();
    }

    /// move list state to top no matter where you are
    fn list_move_top(&mut self) {
        match self.selected_menu {
            MenuItem::Notes => self.notes_state.pos.select(Some(0)),
            MenuItem::Tags => {
                if self.tag_list_state.selected.is_tags() {
                    self.tag_list_state.tags.select(Some(0));
                    self.tag_list_state.notes.select(Some(0));
                } else {
                    self.tag_list_state.notes.select(Some(0));
                }
            }
            MenuItem::Links => {
                if self.link_list_state.selected.is_notes() {
                    self.link_list_state.notes.select(Some(0));
                    self.link_list_state.links.select(Some(0));
                } else {
                    self.link_list_state.links.select(Some(0));
                }
            }
            _ => {}
        }
    }

    /// move list state to bottom no matter where you are
    fn list_move_bottom(&mut self) {
        match self.selected_menu {
            MenuItem::Notes => self
                .notes_state
                .pos
                .select(Some(self.notes_state.notes.len() - 1)),
            MenuItem::Tags => {
                if self.tag_list_state.selected.is_tags() {
                    self.tag_list_state.tags.select(Some(self.get_tags_len()));
                    self.tag_list_state.notes.select(Some(0));
                } else {
                    self.tag_list_state
                        .notes
                        .select(Some(self.get_tag_notes_len()));
                }
            }
            MenuItem::Links => {
                if self.link_list_state.selected.is_notes() {
                    self.link_list_state
                        .notes
                        .select(Some(self.get_link_notes_len()));
                    self.link_list_state.links.select(Some(0));
                } else {
                    self.link_list_state
                        .links
                        .select(Some(self.get_links_len()));
                }
            }
            _ => {}
        }
    }

    /// move list state no matter where you are
    /// positive -- next (1)
    /// negative -- prev (-1)
    fn list_move(&mut self, add_to_selected: i64) {
        if self.input_mode == InputMode::Sort {
            self._sort(add_to_selected)
        } else if self.notebooks_state.is_open {
            self._select_notebook(add_to_selected)
        } else {
            match self.selected_menu {
                MenuItem::Notes => self._move_notes(add_to_selected),
                MenuItem::Tags => self._move_tags(add_to_selected),
                MenuItem::Links => self._move_links(add_to_selected),
                MenuItem::Help => {}
                _ => panic!("create new menu entry move function"),
            }
        }
    }

    fn _sort(&mut self, add_to_selected: i64) {
        if let Some(selected) = self.notes_state.sort_pos.selected() {
            let amount_sorts = self.sort_types.len();
            if amount_sorts != 0 {
                let mut num: i64 = selected as i64 + add_to_selected;
                if num > amount_sorts as i64 - 1 {
                    num = amount_sorts as i64 - 1
                } else if num < 0 as i64 {
                    num = 0
                }
                self.notes_state.sort_pos.select(Some(num as usize));
            }
        }
    }

    fn _select_notebook(&mut self, add_to_selected: i64) {
        if let Some(selected) = self.notebooks_state.current.selected() {
            let amount_notebooks = self.notebooks_state.notebooks.len();
            if amount_notebooks != 0 {
                let mut num: i64 = selected as i64 + add_to_selected;
                if num > amount_notebooks as i64 - 1 {
                    num = amount_notebooks as i64 - 1
                } else if num < 0 as i64 {
                    num = 0
                }
                self.notebooks_state.current.select(Some(num as usize));
            }
        }
    }

    /// helper for list_move
    fn _move_notes(&mut self, add_to_selected: i64) {
        if let Some(selected) = self.notes_state.pos.selected() {
            let amount_notes = self.notes_state.notes.len();
            if amount_notes != 0 {
                // go to first note after last realization
                // self.note_list_state.select(Some((selected as i64 + add_to_selected_note) as usize % amount_notes));
                // Don't go to first note after last realization
                let mut num: i64 = selected as i64 + add_to_selected;
                if num > amount_notes as i64 - 1 {
                    num = amount_notes as i64 - 1
                } else if num < 0 as i64 {
                    num = 0
                }
                self.notes_state.pos.select(Some(num as usize));
            }
        }
    }

    /// helper for list_move
    fn _move_tags(&mut self, add_to_selected: i64) {
        if self.tag_list_state.selected == SelectedTagWidget::Tags {
            if let Some(selected) = self.tag_list_state.tags.selected() {
                let amount_tags = self.get_all_tags().len();
                if amount_tags != 0 {
                    // go to first note after last realization
                    // self.note_list_state.select(Some((selected as i64 + add_to_selected_note) as usize % amount_notes));
                    // Don't go to first note after last realization
                    let mut num: i64 = selected as i64 + add_to_selected;
                    if num > amount_tags as i64 - 1 {
                        num = amount_tags as i64 - 1
                    } else if num < 0 as i64 {
                        num = 0
                    }
                    self.tag_list_state.tags.select(Some(num as usize));
                    self.tag_list_state.notes.select(Some(0));
                }
            }
        } else if self.tag_list_state.selected == SelectedTagWidget::Notes {
            if let Some(selected) = self.tag_list_state.notes.selected() {
                if let Some(notes) = self.get_notes_by_tag(
                    &self.get_all_tags()[self.tag_list_state.tags.selected().unwrap()],
                ) {
                    let amount_notes = notes.len();
                    // go to first note after last realization
                    // self.note_list_state.select(Some((selected as i64 + add_to_selected_note) as usize % amount_notes));
                    // Don't go to first note after last realization
                    let mut num: i64 = selected as i64 + add_to_selected;
                    if num > amount_notes as i64 - 1 {
                        num = amount_notes as i64 - 1
                    } else if num < 0 as i64 {
                        num = 0
                    }
                    self.tag_list_state.notes.select(Some(num as usize));
                }
            }
        }
    }

    /// helper for list_move
    fn _move_links(&mut self, add_to_selected: i64) {
        if self.link_list_state.selected == SelectedLinkWidget::Notes {
            if let Some(selected) = self.link_list_state.notes.selected() {
                let amount_notes = if self.link_list_state.history.is_empty() {
                    self.notes_state.notes.len()
                } else {
                    self.link_list_state
                        .history
                        .last()
                        .expect("last_hist item")
                        .get_notes()
                        .len()
                };
                if amount_notes != 0 {
                    // go to first note after last realization
                    // self.note_list_state.select(Some((selected as i64 + add_to_selected_note) as usize % amount_notes));
                    // Don't go to first note after last realization
                    let mut num: i64 = selected as i64 + add_to_selected;
                    if num > amount_notes as i64 - 1 {
                        num = amount_notes as i64 - 1
                    } else if num < 0 as i64 {
                        num = 0
                    }
                    self.link_list_state.notes.select(Some(num as usize));
                    self.link_list_state.links.select(Some(0));
                }
            }
        } else if self.link_list_state.selected == SelectedLinkWidget::Links {
            if let Some(selected) = self.link_list_state.links.selected() {
                let amount_links: usize = if self.link_list_state.history.is_empty() {
                    self.notes_state.notes[self.link_list_state.notes.selected().unwrap()]
                        .get_note_refs()
                        .len()
                } else {
                    self.link_list_state
                        .history
                        .last()
                        .expect("last HistEnty")
                        .get_notes()
                        .iter()
                        .nth(
                            self.link_list_state
                                .notes
                                .selected()
                                .expect("there allways selected item"),
                        )
                        .expect("selected note")
                        // .selected_note_prev_menu()
                        .get_note_refs()
                        .len()
                };
                if amount_links != 0 {
                    // go to first note after last realization
                    // self.note_list_state.select(Some((selected as i64 + add_to_selected_note) as usize % amount_notes));
                    // Don't go to first note after last realization
                    let mut num: i64 = selected as i64 + add_to_selected;
                    if num > amount_links as i64 - 1 {
                        num = amount_links as i64 - 1
                    } else if num < 0_i64 {
                        num = 0
                    }
                    self.link_list_state.links.select(Some(num as usize));
                }
            }
        }
    }

    fn get_link_notes_len(&self) -> usize {
        let amount_notes = self.notes_state.notes.len() - 1;
        return amount_notes;
    }

    fn get_links_len(&self) -> usize {
        let amount_links = self.notes_state.notes[self.link_list_state.notes.selected().unwrap()]
            .get_note_refs()
            .len()
            - 1;
        return amount_links;
    }

    fn get_tag_notes_len(&self) -> usize {
        if let Some(notes) = self
            .get_notes_by_tag(&self.get_all_tags()[self.tag_list_state.tags.selected().unwrap()])
        {
            let amount_notes = notes.len() - 1;
            return amount_notes;
        }
        return 0;
    }

    fn get_tags_len(&self) -> usize {
        let amount_tags = self.get_all_tags().len() - 1;
        return amount_tags;
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    enable_raw_mode().expect("can run in raw mode");
    execute!(io::stdout(), EnterAlternateScreen)?;

    let (tx, rx) = mpsc::channel();
    let tick_rate = Duration::from_millis(200);
    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));

            if event::poll(timeout).expect("poll works") {
                if let CEvent::Key(key) = event::read().expect("can read events") {
                    tx.send(Event::Input(key)).expect("can send events");
                }
            }

            if last_tick.elapsed() >= tick_rate {
                if let Ok(_) = tx.send(Event::Tick) {
                    last_tick = Instant::now();
                }
            }
        }
    });

    keybindings(rx).expect("keybindings error");

    Ok(())
}

// TODO: separate this file to different parts
