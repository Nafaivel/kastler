use crate::settings::Settings;

pub fn parse_tags_refs_noterefs(
    settings: &Settings,
    contents: String,
) -> (Vec<String>, Vec<String>, Vec<String>) {
    let mut tags: Vec<String> = vec![];

    let mut refs: Vec<String> = vec![];
    let mut note_refs: Vec<String> = vec![];
    let mut footnotes: Vec<String> = vec![];
    let mut temp_ref: String;

    for line in contents.split("\n") {
        temp_ref = String::new();

        for word in line.split(" ") {
            // parse tags
            if word.starts_with("#") && (!word.starts_with("##")) && (word.len() > 1) {
                tags.push(word.to_string());

            // parse links
            } else if word.starts_with("[[") && !word.contains("]]") {
                temp_ref.push_str(&(word.to_string() + " "))
            } else if word.starts_with("[") && !word.contains("]") {
                temp_ref.push_str(&(word.to_string() + " "))
            } else if word.starts_with("[")
                && word.contains(']')
                && word.contains('(')
                && !word.contains(')')
            {
                temp_ref.push_str(word);
            } else if word.starts_with("[[") && word.contains("]]") {
                if settings.brackets_around_link_is_enabled() {
                    // refs.push(word.to_string());
                    note_refs.push(word.to_string());
                } else {
                    note_refs.push(
                        word.trim_start_matches("[[")
                            .trim_end_matches("]]")
                            .to_string(),
                    );
                    // refs.push(
                    //     word.trim_start_matches("[[")
                    //         .trim_end_matches("]]")
                    //         .to_string(),
                    // );
                }
            } else if temp_ref.starts_with("[[") && !temp_ref.contains("]]") {
                temp_ref.push_str(&(word.to_string() + " "))
            } else if temp_ref.starts_with("[") && !temp_ref.contains("]") {
                temp_ref.push_str(&(word.to_string() + " "))
            } else if temp_ref.starts_with("[") && temp_ref.contains("]") && !temp_ref.contains("(")
            {
                temp_ref.push_str(&(word.to_string() + " "))
            } else if temp_ref.starts_with("[")
                && temp_ref.contains("](")
                && !temp_ref.contains(")")
            {
                temp_ref.push_str(&(word.to_string() + " "))
            } else if word.starts_with("[") && word.contains("](") && word.contains(")") {
                if settings.brackets_around_link_is_enabled() {
                    refs.push(word[0..word.find("]").unwrap() + 1].to_string())
                } else {
                    refs.push(
                        word.trim_start_matches("[")[0..word.find("]").unwrap() - 1].to_string(),
                    )
                }
            } else if word.starts_with("[") && word.ends_with("]") {
                footnotes.push(word.to_string());
            }

            // println!("temp_ref: {}", temp_ref);

            if temp_ref.starts_with("[[") && temp_ref.contains("]]") {
                if settings.brackets_around_link_is_enabled() {
                    note_refs.push(temp_ref.to_string());
                    // refs.push(temp_ref.to_string());
                } else {
                    note_refs.push(
                        temp_ref
                            .trim()
                            .trim_start_matches("[[")
                            .trim_end_matches("]]")
                            .to_string(),
                    );
                    // refs.push(
                    //     temp_ref
                    //         .trim()
                    //         .trim_start_matches("[[")
                    //         .trim_end_matches("]]")
                    //         .to_string(),
                    // );
                }
            } else if temp_ref.starts_with("[") && temp_ref.contains("](") && temp_ref.contains(")")
            {
                if settings.brackets_around_link_is_enabled() {
                    // refs.push(temp_ref.to_string());
                    refs.push(
                        temp_ref[0..temp_ref.find("]").unwrap() + 1]
                            .to_string()
                            .clone(),
                    );
                    // println!("{}", temp_ref);
                    temp_ref = String::new();
                } else {
                    // refs.push(temp_ref.to_string());
                    refs.push(
                        temp_ref.trim_start_matches("[")[0..temp_ref.find("]").unwrap() - 1]
                            .to_string()
                            .clone(),
                    );
                    temp_ref = String::new();
                }
            }
        }
        // println!("line")
    }

    // check filepath in note_refs
    for (i, refer) in note_refs.clone().iter().enumerate() {
        note_refs[i] = refer.trim().to_string();
        if refer == "[[]]" || refer == "[[.]]" || refer == "[[./]]" {
            note_refs.remove(i);
        }
    }
    return (tags, refs, note_refs);
}
