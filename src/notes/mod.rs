use chrono::prelude::*;
use rand::{distributions::Alphanumeric, prelude::*};
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::PathBuf;

use crate::db;
// use crate::settings;
use crate::settings::Settings;
use crate::App;
use crate::Error;

mod md_parser;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Note {
    name: String,
    date: String,
    path: String,
    tags: Vec<String>,
    // references to other files
    refs: Vec<String>,
    note_refs: Vec<String>,
}

impl Note {
    pub fn new(
        name: String,
        date: String,
        path: String,
        tags: Vec<String>,
        refs: Vec<String>,
        note_refs: Vec<String>,
    ) -> Note {
        Note {
            name,
            date,
            path,
            tags,
            refs,
            note_refs,
        }
    }

    /// creates if not exists otherwise return parsed note
    pub fn create_from_filename(settings: &Settings, path: &str) -> Note {
        let mut fullpath2note = settings.get_notes_dir();
        // notes_dir.set_file_name(path);
        fullpath2note.push(path);
        if !fullpath2note.exists() {
            fs::File::create(fullpath2note).expect("create failed");
        }

        return Note::new_from_filename(settings, path).unwrap();
    }

    /// check if file exists and it's not an dir then returns Note else returns None
    pub fn new_from_filename(settings: &Settings, filename: &str) -> Option<Note> {
        let mut file = settings.get_notes_dir();
        file.push(filename);

        if file.exists() && !file.is_dir() {
            return Some(Self::new_from_file(settings, &file));
        } else {
            return None;
        }
    }

    /// parse and return note
    pub fn new_from_file(settings: &Settings, path: &PathBuf) -> Note {
        let filename = path.file_name().unwrap().to_str().unwrap();

        // get name and date
        let name: String;
        let date: String;
        let date_length = settings.get_date_length();
        if filename.len() > date_length {
            let str = filename[0..date_length].to_string();
            date = str
                .parse::<i32>()
                .unwrap_or(0)
                .to_string()
                .trim_end_matches("0")
                .to_string();
            name = filename[date_length + 1..filename.len()]
                .to_string()
                .trim_end_matches(".md")
                .to_string();
        } else {
            date = String::new();
            name = filename[0..filename.len()]
                .to_string()
                .trim_end_matches(".md")
                .to_string();
        }

        let contents = fs::read_to_string(path).expect("Should have been able to read the file");

        let (tags, refs, note_refs);

        match path.extension() {
            // TODO: add org implementation here
            Some(ext) => if ext == "md" {},
            None => {}
        }

        (tags, refs, note_refs) = md_parser::parse_tags_refs_noterefs(settings, contents);

        Note {
            name,
            date,
            path: path
                .to_str()
                .unwrap()
                .to_owned()
                .trim_start_matches(settings.get_notes_dir().to_str().unwrap())
                .trim_start_matches('/')
                .to_string(),
            tags,
            refs,
            note_refs,
        }
    }

    pub fn get_name(&self) -> String {
        self.name.clone()
    }

    pub fn get_date(&self) -> String {
        self.date.clone()
    }

    pub fn get_path(&self) -> String {
        self.path.clone()
    }

    pub fn get_full_path(&self, settings: &Settings) -> String {
        let mut nd = settings.get_notes_dir();
        nd.push(self.path.clone());
        nd.to_str().unwrap().to_string()
    }

    pub fn get_tags(&self) -> Vec<String> {
        self.tags.clone()
    }

    pub fn rename_tag(&mut self, settings: &Settings, old_name: &String, new_name: &String) {
        if self.tags.contains(&old_name) {
            // let tag_pos = self.tags.binary_search(&old_name).unwrap();
            let mut tag_pos: Option<usize> = None;
            for (pos, tag) in self.tags.iter().enumerate() {
                if tag == old_name {
                    tag_pos = Some(pos);
                    break;
                }
            }
            // if tag exists
            if tag_pos.is_some() {
                // rename tag in note
                self.tags[tag_pos.unwrap()] = new_name.clone();
                // rename tag in file
                let mut contents = fs::read_to_string(self.get_full_path(settings)).unwrap();
                // unwrap_or("fail".to_string());
                if contents == "fail" {
                    return;
                } else {
                    contents = contents.replace(old_name, new_name);
                    fs::write(self.get_full_path(settings), contents)
                        .expect("failed to rename tag(write err)");
                }
            }
        }
    }

    pub fn get_refs(&self) -> Vec<String> {
        self.refs.clone()
    }

    pub fn get_note_refs(&self) -> Vec<String> {
        self.note_refs.clone()
    }

    pub fn add_to_db(app: &mut App, name: String) -> Result<Vec<Note>, Error> {
        let db_content = fs::read_to_string(db::get_db_path())?;
        let mut parsed: Vec<Note> = serde_json::from_str(&db_content)?;

        let date = Local::now()
            .format(&app.settings.get_date_format())
            .to_string();

        let mut path = app.notebooks_state.seleced_notebook_dir(&app.settings);
        path.push(format!("{}_{}.md", date, name));

        let new_note = Note {
            name,
            date,
            path: path
                .to_str()
                .unwrap()
                .trim_start_matches(app.settings.get_notes_dir().to_str().unwrap())
                .trim_start_matches("/")
                .to_string(),
            refs: vec![],
            tags: vec![],
            note_refs: vec![],
        };

        parsed.push(new_note.clone());
        fs::write(db::get_db_path(), &serde_json::to_vec(&parsed)?)?;
        app.notes_state.notes.push(new_note);
        Ok(parsed)
    }

    pub fn rename(app: &mut App, new_filename: String) {
        let mut note = app.settings.get_notes_dir();
        let note_path = (&app.notes_state.selected_note().get_path())
            .trim_start_matches("./")
            .trim_start_matches("/")
            .trim_start_matches(".")
            .to_string();
        note.push(note_path.clone());
        // note.set_file_name("");
        // let dir: String = dir
        //     .to_str()
        //     .expect("failed to convert")
        //     .trim_end_matches("/")
        //     .to_string()
        //     + "/";
        let mut new_note = note.clone();
        new_note.set_file_name(&new_filename);
        // there just mv and change in Note
        fs::rename(note.clone().to_str().unwrap(), new_note.to_str().unwrap())
            .expect("failed to rename");
        // app.update_notes();
        app.notes_state.notes.insert(
            app.notes_state.pos.selected().unwrap(),
            Note::new_from_file(&app.settings, &PathBuf::from(new_note.to_str().unwrap())),
        );
        app.notes_state
            .notes
            .remove(app.notes_state.pos.selected().unwrap() + 1);
    }

    pub fn _add_random_to_db() -> Result<Vec<Note>, Error> {
        let rng = rand::thread_rng();
        let db_content = fs::read_to_string(db::get_db_path())?;
        let mut parsed: Vec<Note> = serde_json::from_str(&db_content)?;

        let random_note = Note {
            name: rng.sample_iter(Alphanumeric).take(10).collect(),
            date: Local::now().format("%Y%m%d").to_string(),
            path: String::new(),
            refs: vec![String::new()],
            tags: vec![String::new()],
            note_refs: vec![String::new()],
        };

        parsed.push(random_note);
        fs::write(db::get_db_path(), &serde_json::to_vec(&parsed)?)?;
        Ok(parsed)
    }
}
