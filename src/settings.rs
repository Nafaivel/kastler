use std::collections::hash_map::HashMap;
use std::path::PathBuf;

use serde::{Deserialize, Serialize};

use crate::keybindings::Keybindings;
use crate::MenuItem;

#[derive(Debug, Serialize, Deserialize)]
pub struct SettingsBuilder {
    path2notes: Option<String>,
    date_format: Option<String>,
    editor: Option<String>,
    editor_args: Option<Vec<String>>,
    brackets_around_link_is_enabled: Option<bool>,
    exit_after_changing_note: Option<bool>,
    open_note_after_creating: Option<bool>,
    langmaps: Option<HashMap<String, String>>,
    start_menu: Option<MenuItem>,
    keybindings: Option<Keybindings>,
}

impl SettingsBuilder {
    fn parse_settings() -> Settings {
        let mut cfg_dir = dirs::config_dir().unwrap();
        cfg_dir.push("kastler/");
        let mut cfg_dir2 = dirs::home_dir().unwrap();
        cfg_dir2.push(".kastler/");
        let mut paths: Vec<PathBuf> = vec![cfg_dir, cfg_dir2];

        for path in &mut paths {
            if path.exists() {
                path.push("config.yml");
                if path.exists() {
                    let f = std::fs::File::open(path).expect("Could not open file.");
                    let settings: SettingsBuilder =
                        serde_yaml::from_reader(f).expect("Could not read values.");
                    return settings.build();
                }
            }
        }
        Settings::default()
    }

    fn build(self) -> Settings {
        let default = Settings::default();

        Settings {
            path2notes: match self.path2notes {
                Some(val) => val,
                None => default.path2notes,
            },
            date_format: match self.date_format {
                Some(val) => val,
                None => default.date_format,
            },
            editor: self.editor,
            editor_args: match self.editor_args {
                Some(val) => val,
                None => default.editor_args,
            },
            brackets_around_link_is_enabled: match self.brackets_around_link_is_enabled {
                Some(val) => val,
                None => default.brackets_around_link_is_enabled,
            },
            exit_after_changing_note: match self.exit_after_changing_note {
                Some(val) => val,
                None => default.exit_after_changing_note,
            },
            open_note_after_creating: match self.open_note_after_creating {
                Some(val) => val,
                None => default.open_note_after_creating,
            },
            langmaps: match self.langmaps {
                Some(val) => {
                    if val.contains_key("en") {
                        val
                    } else {
                        let mut val = val;
                        val.insert(
                            "en".to_string(),
                            "qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?"
                                .to_string(),
                        );
                        val
                    }
                }
                None => default.langmaps,
            },
            start_menu: match self.start_menu {
                Some(val) => val,
                None => default.start_menu,
            },
            keybindings: match self.keybindings {
                Some(val) => val,
                None => default.keybindings,
            },
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Settings {
    path2notes: String,
    date_format: String,
    editor: Option<String>,
    editor_args: Vec<String>,
    brackets_around_link_is_enabled: bool,
    exit_after_changing_note: bool,
    open_note_after_creating: bool,
    langmaps: HashMap<String, String>,
    start_menu: MenuItem,
    keybindings: Keybindings,
    // TODO: add here main fyletype option via enum with org  and md variants
}
// TODO: add ability to change language

impl Settings {
    pub fn get_settings() -> Settings {
        SettingsBuilder::parse_settings()
        // Self::parse_settings()
    }

    pub fn default() -> Settings {
        Settings {
            path2notes: "~/Documents/notes".to_string(),
            // path2notes: "~/development/rust/kastler/test_notes/".to_string(),
            start_menu: MenuItem::Links,
            date_format: "%Y%m%d".to_string(),
            editor: None,
            editor_args: vec![],
            brackets_around_link_is_enabled: true,
            exit_after_changing_note: false,
            open_note_after_creating: true,
            langmaps: parse_langmaps(),
            keybindings: Keybindings::new(),
        }
    }

    fn parse_settings() -> Settings {
        let mut cfg_dir = dirs::config_dir().unwrap();
        cfg_dir.push("kastler/");
        let mut cfg_dir2 = dirs::home_dir().unwrap();
        cfg_dir2.push(".kastler/");
        let mut paths: Vec<PathBuf> = vec![cfg_dir, cfg_dir2];

        for path in &mut paths {
            if path.exists() {
                path.push("config.yml");

                let f = std::fs::File::open(path).expect("Could not open file.");
                let settings: Settings =
                    serde_yaml::from_reader(f).expect("Could not read values.");
                return settings;
            }
        }
        println!("{:#?}", Self::default());
        Self::default()
    }

    /// returns start_menu from Settings
    pub fn get_start_menu(&self) -> MenuItem {
        self.start_menu
    }

    pub fn get_date_format(&self) -> String {
        self.date_format.clone()
    }

    pub fn get_date_length(&self) -> usize {
        let mut length = 0;
        if self.get_date_format().split('%').count() != 1_usize {
            for statement in self.get_date_format().split('%') {
                if statement.len() == 1 {
                    match statement {
                        "Y" => length += 4,
                        "m" => length += 2,
                        "d" => length += 2,
                        _ => {}
                    }
                } else {
                    let mut is_first_chr = true;
                    for chr in statement.chars() {
                        if is_first_chr {
                            match chr {
                                'Y' => length += 4,
                                'm' => length += 2,
                                'd' => length += 2,
                                _ => {}
                            }
                            is_first_chr = false;
                        } else {
                            length += 1;
                        }
                    }
                }
            }
        } else {
            length = self.get_date_format().len();
        }
        length
    }

    pub fn get_notes_dir(&self) -> PathBuf {
        let mut notes = PathBuf::new();
        for part in self.path2notes.split('/') {
            if part == "~" {
                notes.push(dirs::home_dir().unwrap());
            } else {
                notes.push(part);
            }
        }
        notes
    }

    pub fn get_editor(&self) -> String {
        if self.editor.is_some() {
            Settings::get_settings().editor.unwrap()
        } else {
            "xdg-open".to_string()
        }
    }

    pub fn get_editor_args(&self) -> Vec<String> {
        self.editor_args.clone()
    }

    pub fn brackets_around_link_is_enabled(&self) -> bool {
        self.brackets_around_link_is_enabled
    }

    pub fn exit_after_changing_note_is_enabled(&self) -> bool {
        self.exit_after_changing_note
    }

    pub fn open_note_after_creating_is_enabled(&self) -> bool {
        self.open_note_after_creating
    }

    pub fn get_langmaps(&self) -> HashMap<String, String> {
        self.langmaps.clone()
    }
}

fn parse_langmaps() -> HashMap<String, String> {
    HashMap::from([
        (
            "en".to_string(),
            "qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?".to_string(),
        ),
        (
            "be".to_string(),
            "йцукенгшўзх'фывапролджэячсмітьбю.ЙЦУКЕНГШЎЗХ'ФЫВАПРОЛДЖЭЯЧСМІТЬБЮ,".to_string(),
        ),
        // (
        //     "ru".to_string(),
        //     "йцукенгшщзхъфывапролджэячсмитьбю. ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,".to_string(),
        // ),
    ])
}
