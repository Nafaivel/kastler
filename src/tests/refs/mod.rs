#[cfg(test)]
mod tests {

    use crate::{notes, settings};
    use std::path::PathBuf;

    #[test]
    fn test_note_from_file_compare_refs() {
        let settings = settings::Settings::get_settings();
        let mut path: PathBuf = PathBuf::new();
        path.push(dirs::home_dir().unwrap());
        path.push("development/rust/kastler/src/tests/refs/22222222_test.md");
        let note = notes::Note::new_from_file(&settings, &path);

        let mut refs = vec![];
        let mut note_refs: Vec<String> = vec![];
        // prepare links
        if settings.brackets_around_link_is_enabled() {
            refs.push("[simple link]".to_string());
            refs.push("[simpla link with title]".to_string());
            refs.push("[rel link to file]".to_string());
        } else {
            refs.push("simple link".to_string());
            refs.push("simpla link with title".to_string());
            refs.push("rel link to file".to_string());
        }

        assert_eq!(
            note.get_refs(),
            notes::Note::new(
                String::from("test"),
                String::from("22222222"),
                String::from("22222222_test.md"),
                vec![],
                refs,
                note_refs
            )
            .get_refs()
        )
    }

    #[test]
    fn test_note_from_file_compare_note_refs() {
        let settings = settings::Settings::get_settings();
        let mut path: PathBuf = PathBuf::new();
        path.push(dirs::home_dir().unwrap());
        path.push("development/rust/kastler/src/tests/refs/22222222_test.md");
        let note = notes::Note::new_from_file(&settings, &path);

        let refs = vec![];
        let mut note_refs: Vec<String> = vec![];
        // prepare links
        if settings.brackets_around_link_is_enabled() {
            note_refs.push("[[link]]".to_string());
            note_refs.push("[[link wit space]]".to_string());
        } else {
            note_refs.push("link".to_string());
            note_refs.push("link wit space".to_string());
        }

        assert_eq!(
            note.get_note_refs(),
            notes::Note::new(
                String::from("test"),
                String::from("22222222"),
                String::from("22222222_test.md"),
                vec![],
                refs,
                note_refs
            )
            .get_note_refs()
        )
    }

    // #[test]
    fn test_note_from_file_compare_refs_with_footnotes() {
        let settings = settings::Settings::get_settings();
        let mut path: PathBuf = PathBuf::new();
        path.push(dirs::home_dir().unwrap());
        path.push("development/rust/kastler/src/tests/refs/22222222_test.md");
        let note = notes::Note::new_from_file(&settings, &path);
        assert_eq!(
            note.get_refs(),
            notes::Note::new(
                String::from("test"),
                String::from("22222222"),
                String::from("22222222_test.md"),
                vec![],
                vec![
                    "simple link".to_string(),
                    "simpla link with title".to_string(),
                    "link".to_string(),
                    "link wit space".to_string(),
                    "link with footnote".to_string(),
                    "rel link to file".to_string(),
                    "numberik footnote".to_string(),
                    "sqr text link".to_string(),
                ],
                vec![]
            )
            .get_refs()
        )
    }
}
