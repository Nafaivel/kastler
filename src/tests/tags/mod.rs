#[cfg(test)]
mod tests {

    use crate::{notes, settings};
    use std::path::PathBuf;

    #[test]
    fn test_note_from_file_compare_tags() {
        let mut settings = settings::Settings::get_settings();
        let mut path: PathBuf = PathBuf::new();
        path.push(dirs::home_dir().unwrap());
        path.push("development/rust/kastler/src/tests/tags/22222222_test.md");
        let note = notes::Note::new_from_file(&settings, &path);
        assert_eq!(
            note.get_tags(),
            notes::Note::new(
                String::from("test"),
                String::from("22222222"),
                String::from("22222222_test.md"),
                vec![
                    "#test".to_string(),
                    "#works".to_string(),
                    "#normally".to_string()
                ],
                vec![],
                vec![]
            )
            .get_tags()
        )
    }
}
