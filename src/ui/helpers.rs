use tui::{
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::Style,
    widgets::{Block, Borders, Paragraph},
};

/// helper function to create a centered rect using up certain % of the available rect `r`
pub fn centered_rect_in_persents(x_persent: u16, y_persent: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - y_persent) / 2),
                Constraint::Percentage(y_persent),
                Constraint::Percentage((100 - y_persent) / 2),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - x_persent) / 2),
                Constraint::Percentage(x_persent),
                Constraint::Percentage((100 - x_persent) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}

/// helper function to create a centered rect using up certain len of the available rect `r`
pub fn centered_rect_in_pt(size_x: u16, size_y: u16, r: Rect) -> Rect {
    let persent_x = (size_x as f32 / (r.width as f32 / 100.0)) as u16;
    let persent_y = (size_y as f32 / (r.height as f32 / 100.0)) as u16;
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - persent_y) / 2),
                Constraint::Length(size_y),
                Constraint::Percentage((100 - persent_y) / 2),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - persent_x) / 2),
                Constraint::Length(size_x),
                Constraint::Percentage((100 - persent_x) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}

/// helper for creating borders
pub fn just_borders<'a>() -> Paragraph<'a> {
    let borders = Paragraph::new(vec![])
        .style(Style::default())
        .alignment(Alignment::Center)
        .block(Block::default().borders(Borders::ALL));
    borders
}
