use std::io::stdout;

use crossterm::execute;
use tui::{
    backend::Backend,
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, Clear, List, ListItem, Paragraph, Tabs, Wrap},
    Frame,
};

use crate::{
    App, Confirm, EditingIntent, EditingMode, InputMode, LinkListState, MenuItem, Note,
    SelectedLinkWidget, SelectedTagWidget, SortType,
};

mod helpers;
use helpers::*;
mod notes_menu;
use notes_menu::*;

// struct Colors {
//     menu_fg: Color,
//     menu_bg: Color,
// }

pub fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    if app.input_mode == InputMode::Wait {
        return;
    }

    let size = f.size();
    let mut constraints = vec![Constraint::Length(3), Constraint::Min(2)];
    if app.input_mode == InputMode::Editing {
        constraints.push(Constraint::Length(3));
    }
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        // .margin(1)
        .constraints(constraints.as_ref())
        .split(size);

    // TODO: check all STRING and replace it with localized.body or smth lik that
    // STRING
    let menu_titles = vec!["notes", "?help", "tags", "Links", "quit"];

    let menu = menu_titles
        .iter()
        .map(|t| {
            let (first, rest) = t.split_at(1);
            Spans::from(vec![
                Span::styled(
                    first,
                    Style::default()
                        .fg(Color::Yellow)
                        .add_modifier(Modifier::UNDERLINED),
                ),
                Span::styled(rest, Style::default().fg(Color::White)),
            ])
        })
        .collect();

    let tabs = Tabs::new(menu)
        .select(app.selected_menu.into())
        // STRING
        .block(Block::default().title("Menu").borders(Borders::ALL))
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().fg(Color::Yellow))
        .divider(Span::raw("|"));

    f.render_widget(tabs, chunks[0]);
    match app.selected_menu {
        MenuItem::Help => f.render_widget(render_help(), chunks[1]),
        MenuItem::Tags => {
            let notes_chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
                .split(chunks[1]);
            let (left, right) = render_tags(app);
            if app.tag_list_state.selected == SelectedTagWidget::Tags {
                f.render_stateful_widget(left, notes_chunks[0], &mut app.tag_list_state.tags);
                f.render_widget(right, notes_chunks[1]);
            } else if app.tag_list_state.selected == SelectedTagWidget::Notes {
                f.render_stateful_widget(left, notes_chunks[0], &mut app.tag_list_state.tags);
                f.render_stateful_widget(right, notes_chunks[1], &mut app.tag_list_state.notes);
            }
        }
        MenuItem::Links => {
            let notes_chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
                .split(chunks[1]);
            let (left, right) = render_links(app);
            if app.link_list_state.selected == SelectedLinkWidget::Notes {
                f.render_stateful_widget(left, notes_chunks[0], &mut app.link_list_state.notes);
                f.render_widget(right, notes_chunks[1]);
            } else if app.link_list_state.selected == SelectedLinkWidget::Links {
                f.render_stateful_widget(left, notes_chunks[0], &mut app.link_list_state.notes);
                f.render_stateful_widget(right, notes_chunks[1], &mut app.link_list_state.links);
            }
        }
        MenuItem::Notes => {
            let notes_chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Length(35), Constraint::Min(1)].as_ref())
                .split(chunks[1]);
            let (left, right) = render_notes(app);
            f.render_stateful_widget(left, notes_chunks[0], &mut app.notes_state.pos);
            f.render_widget(right, notes_chunks[1]);
        }
        _ => panic!("active menu item"),
    }
    if app.notebooks_state.is_open {
        let borders_area = centered_rect_in_persents(55, 80, size);
        f.render_widget(Clear, borders_area); //this clears out the background
        f.render_widget(just_borders(), borders_area);
        let mut area = borders_area.clone();

        // change position of main widgets
        area.x += 1;
        area.y += 1;
        // change size of main widgets
        area.width -= 2;
        area.height -= 2;

        // draw notes
        let notebook_chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(3), Constraint::Min(3)].as_ref())
            .split(area);
        let (up, down) = notebook_select(app);
        f.render_widget(up, notebook_chunks[0]);
        f.render_stateful_widget(down, notebook_chunks[1], &mut app.notebooks_state.current);
    }

    match app.input_mode {
        InputMode::Sort => {
            let borders_area = centered_rect_in_pt(40, 10, size);
            f.render_widget(Clear, borders_area); //this clears out the background
            f.render_widget(just_borders(), borders_area);
            let mut area = borders_area.clone();

            // change position of main widgets
            area.x += 1;
            area.y += 1;
            // change size of main widgets
            area.width -= 2;
            area.height -= 2;

            // draw notes
            let sort_chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints([Constraint::Length(3), Constraint::Min(3)].as_ref())
                .split(area);
            let (up, down) = sort_select(app);
            f.render_widget(up, sort_chunks[0]);
            f.render_stateful_widget(down, sort_chunks[1], &mut app.notes_state.sort_pos);
        }
        InputMode::Editing => {
            let title;
            match app.editing_state.intent {
                // STRING
                EditingIntent::NoteName => title = "Note name",
                // STRING
                EditingIntent::ChangeNoteName => title = "Rename note",
                // STRING
                EditingIntent::ChangeTagName => title = "Rename tag",
                // STRING
                EditingIntent::NotebookName => title = "Rename notebook",
                // STRING
                _ => title = "Input",
            }
            let input = Paragraph::new(app.editing_state.input.as_ref())
                .style(match app.input_mode {
                    InputMode::Editing => Style::default().fg(Color::Yellow),
                    _ => Style::default(),
                })
                .block(Block::default().borders(Borders::ALL).title(title));
            f.render_widget(input, chunks[2]);
            f.set_cursor(
                // Put cursor past the end of the input text
                chunks[2].x + app.editing_state.selected_index as u16 + 1,
                // Move one line down, from the border to the input line
                chunks[2].y + 1,
            );
            match app.editing_state.mode {
                EditingMode::Insert => {
                    execute!(stdout(), crossterm::cursor::SetCursorStyle::BlinkingBar).unwrap()
                }
                EditingMode::Normal => {
                    execute!(stdout(), crossterm::cursor::SetCursorStyle::BlinkingBlock).unwrap()
                }
            }
        }
        InputMode::Confirm => {
            let area = centered_rect_in_pt(30, 7, size);
            f.render_widget(Clear, area); //this clears out the background
            f.render_widget(confirm(app), area);
        }
        _ => {}
    }
    if app.input_mode != InputMode::Editing {
        execute!(
            stdout(),
            crossterm::cursor::SetCursorStyle::DefaultUserShape
        )
        .unwrap();
    }
}

fn confirm<'a>(app: &'a App) -> Paragraph<'a> {
    let confirm = Paragraph::new(match_confirm_text(app))
        .style(Style::default())
        .alignment(Alignment::Center)
        .block(Block::default().borders(Borders::ALL));
    confirm
}

fn match_confirm_text(app: &App) -> Vec<Spans> {
    let mut strings = vec![];
    let question: String;
    let name: String;
    match app.confirm {
        Confirm::DeleteNote => {
            // STRING
            question = "Delete note?".to_string();
            strings.push(Spans::from(vec![Span::raw(question)]));
            name = app.notes_state.notes[app.notes_state.pos.selected().unwrap_or(0)].get_name();
            strings.push(Spans::from(vec![Span::raw("")]));
            strings.push(Spans::from(vec![Span::raw(name)]));
            strings.push(Spans::from(vec![Span::raw("")]));
        }
        Confirm::DeleteNotebook => {
            // STRING
            question = "Delete notebook?".to_string();
            strings.push(Spans::from(vec![Span::raw(question)]));
        }
        Confirm::None => {
            todo!()
        }
    }
    // STRING
    strings.push(Spans::from(vec![Span::raw("y/n")]));
    return strings;
}

fn sort_select<'a>(app: &mut App) -> (Paragraph<'a>, List<'a>) {
    // STRING
    let label = Paragraph::new(vec![Spans::from(vec![Span::raw("Select sort type")])])
        .style(Style::default())
        .alignment(Alignment::Center)
        .wrap(Wrap { trim: true })
        .block(
            Block::default()
                .border_type(BorderType::Plain)
                .borders(Borders::NONE),
        );
    let sort_block = Block::default()
        .borders(Borders::NONE)
        .style(Style::default().fg(Color::White));

    // STRING
    let default_sort = String::from("Default");

    let sort_list = app.sort_types.clone();
    let mut items: Vec<_> = vec![ListItem::new(Spans::from(vec![Span::styled(
        default_sort,
        Style::default(),
    )]))];

    if !sort_list.is_empty() {
        items.pop();

        for sort in sort_list.clone() {
            items.push(ListItem::new(Spans::from(vec![Span::styled(
                match sort {
                    // STRING
                    SortType::ByDate => "by date",
                    // STRING
                    SortType::ByDateRev => "by date reverse",
                    // STRING
                    SortType::ByName => "by name",
                    // STRING
                    SortType::ByNameRev => "by name reverse",
                    _ => panic!("sort type menu item not exists"),
                },
                Style::default(),
            )])));
        }
    }

    let notebook_list_widget = List::new(items).block(sort_block).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );
    (label, notebook_list_widget)
}

fn notebook_select<'a>(app: &mut App) -> (Paragraph<'a>, List<'a>) {
    // STRING
    let label = Paragraph::new(vec![Spans::from(vec![Span::raw("Select notbook")])])
        .style(Style::default())
        .alignment(Alignment::Center)
        .wrap(Wrap { trim: true })
        .block(
            Block::default()
                .border_type(BorderType::Plain)
                .borders(Borders::NONE),
        );
    let notebook_block = Block::default()
        .borders(Borders::NONE)
        .style(Style::default().fg(Color::White));

    // STRING
    let default_notebook = String::from("Default");

    let notebook_list = app.notebooks_state.get_notebooks();
    let mut items: Vec<_> = vec![ListItem::new(Spans::from(vec![Span::styled(
        default_notebook,
        Style::default(),
    )]))];

    if !notebook_list.is_empty() {
        items.pop();

        for notebook in notebook_list.clone() {
            items.push(ListItem::new(Spans::from(vec![Span::styled(
                notebook,
                Style::default(),
            )])));
        }
    }

    let notebook_list_widget = List::new(items).block(notebook_block).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );
    (label, notebook_list_widget)
}

fn render_help<'a>() -> Paragraph<'a> {
    let help = Paragraph::new(vec![
        Spans::from(vec![Span::raw("")]),
    // STRING
        Spans::from(vec![Span::raw("Welcome")]),
        Spans::from(vec![Span::raw("")]),
        Spans::from(vec![Span::raw("to")]),
        Spans::from(vec![Span::raw("")]),
        Spans::from(vec![Span::styled(
            "kastler",
            Style::default().fg(Color::LightBlue),
        )]),
        Spans::from(vec![Span::raw("")]),
    // STRING
        Spans::from(vec![Span::raw("Press 'n' to access notes, 'a' to add random new note and 'd' to delete the currently selected note.")]),
    ])
        .alignment(Alignment::Left)
        .wrap(Wrap{ trim: true })
        .block(
            Block::default()
                .borders(Borders::ALL)
                .style(Style::default().fg(Color::White))
                .title("Help")
                .border_type(BorderType::Plain),
        );
    help
}

fn render_tags<'a>(app: &mut App) -> (List<'a>, List<'a>) {
    let tags = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Tags")
        .border_type(BorderType::Plain);

    // STRING
    let empty_tag = String::from("None");

    let tag_list = app.get_all_tags();
    let mut items: Vec<_> = vec![ListItem::new(Spans::from(vec![Span::styled(
        empty_tag.clone(),
        Style::default(),
    )]))];

    if !tag_list.is_empty() {
        items.pop();

        for tag in tag_list.clone() {
            items.push(ListItem::new(Spans::from(vec![Span::styled(
                tag,
                Style::default(),
            )])));
        }
    }

    let tag_list_widget = List::new(items).block(tags).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );

    let notes = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Notes")
        .border_type(BorderType::Plain);

    let empty_note = Note::new(
        // STRING
        "None".to_string(),
        "".to_string(),
        "".to_string(),
        vec![],
        vec![],
        vec![],
    );

    let mut items: Vec<_> = vec![ListItem::new(Spans::from(vec![Span::styled(
        empty_note.get_name(),
        Style::default(),
    )]))];

    if !tag_list.is_empty() {
        if let Some(note_list) =
            app.get_notes_by_tag(&tag_list[app.tag_list_state.tags.selected().unwrap()])
        {
            items.pop();

            for note in note_list {
                items.push(ListItem::new(Spans::from(vec![Span::styled(
                    note.get_name(),
                    Style::default(),
                )])));
            }
        }
    }

    let note_list_widget = List::new(items).block(notes).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );

    return (tag_list_widget, note_list_widget);
}

fn render_links<'a>(app: &mut App) -> (List<'a>, List<'a>) {
    let (note_items, link_items) = link_menu_get_items(app);

    let notes = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        // STRING
        .title("Notes")
        .border_type(BorderType::Plain);

    let note_list_widget = List::new(note_items).block(notes).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );

    let links = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Links")
        .border_type(BorderType::Plain);

    let link_list_widget = List::new(link_items).block(links).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );

    return (note_list_widget, link_list_widget);
}

fn link_menu_get_items(app: &App) -> (Vec<ListItem<'static>>, Vec<ListItem<'static>>) {
    let mut note_items: Vec<ListItem> = vec![];

    let empty_note = Note::new(
        // STRING
        "None".to_string(),
        "".to_string(),
        "".to_string(),
        vec![],
        vec![],
        vec![],
    );

    let empty_nvec = vec![empty_note];

    let notes: &Vec<Note> = if !app.link_list_state.history.is_empty() {
        app.link_list_state
            .history
            .last()
            .expect("last history entry")
            .get_notes()
    } else if !app.notes_state.notes.is_empty() {
        &app.notes_state.notes
    } else {
        &empty_nvec
    };

    let link_list: Vec<String> = if notes.is_empty() {
        // STRING
        vec!["None".to_string()]
    } else {
        notes[app
            .link_list_state
            .notes
            .selected()
            .expect("selected_note id")]
        .get_note_refs()
        .into_iter()
        .map(LinkListState::normalize_link)
        .collect()
    };

    for note in notes {
        note_items.push(ListItem::new(Spans::from(vec![Span::styled(
            note.get_name(),
            Style::default(),
        )])));
    }

    // STRING
    let empty_link = String::from("None");

    // let link_list = app.link_list_state.selected_note_normalized_link(app);

    let mut link_items: Vec<_> = vec![ListItem::new(Spans::from(vec![Span::styled(
        empty_link,
        Style::default(),
    )]))];

    if !link_list.is_empty() {
        link_items.pop();

        for link in link_list {
            link_items.push(ListItem::new(Spans::from(vec![Span::styled(
                link,
                Style::default(),
            )])));
        }
    }

    return (note_items, link_items);
}
