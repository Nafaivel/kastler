pub(crate) use crate::App;
use crate::Note;
use tui::{
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, List, ListItem, Paragraph, Wrap},
};

pub fn render_notes<'a>(app: &mut App) -> (List<'a>, Paragraph<'a>) {
    let notes = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Notes")
        .border_type(BorderType::Plain);

    let empty_note = Note::new(
        "(None)".to_string(),
        "Never".to_string(),
        "yuu need to create any note".to_string(),
        vec![],
        vec![],
        vec![],
    );

    let note_list = app.notes_state.notes.clone();
    let mut items: Vec<_> = vec![ListItem::new(Spans::from(vec![Span::styled(
        empty_note.get_name(),
        Style::default(),
    )]))];

    let mut selected_note = empty_note;

    if !note_list.is_empty() {
        items.pop();

        for note in &note_list {
            items.push(ListItem::new(Spans::from(vec![Span::styled(
                note.get_name(),
                Style::default(),
            )])));
        }

        selected_note = note_list
            .get(
                app.notes_state
                    .pos
                    .selected()
                    .expect("there is always a selected note"),
            )
            .expect("not exists")
            .clone();
    }

    let list = List::new(items).block(notes).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );

    let notes = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Note─detail")
        .border_type(BorderType::Plain);
    let mut note_description: Vec<_> = vec![
        Spans::from(vec![Span::styled("name:", Style::default())]),
        Spans::from(vec![Span::styled(
            selected_note.get_name(),
            Style::default(),
        )]),
    ];
    if selected_note.get_date() != "" {
        note_description.push(Spans::from(vec![Span::styled("date:", Style::default())]));
        note_description.push(Spans::from(vec![Span::styled(
            selected_note.get_date(),
            Style::default(),
        )]));
    }
    if selected_note.get_path() != "" {
        note_description.push(Spans::from(vec![Span::styled("path:", Style::default())]));
        note_description.push(Spans::from(vec![Span::styled(
            selected_note.get_path(),
            Style::default(),
        )]));
    }
    if !selected_note.get_tags().is_empty() {
        let mut str_tags = String::new();
        for tag in selected_note.get_tags() {
            str_tags += &(tag + &", ");
        }
        str_tags = str_tags.trim_end_matches(", ").to_string();
        note_description.push(Spans::from(vec![Span::styled("tags:", Style::default())]));
        note_description.push(Spans::from(vec![Span::styled(str_tags, Style::default())]));
    }
    if !selected_note.get_note_refs().is_empty() {
        let mut str_refs = String::new();
        for note_ref in selected_note.get_note_refs() {
            str_refs += &(note_ref + &", ");
        }
        str_refs = str_refs.trim_end_matches(", ").to_string();
        note_description.push(Spans::from(vec![Span::styled(
            "note refs:",
            Style::default(),
        )]));
        note_description.push(Spans::from(vec![Span::styled(str_refs, Style::default())]));
    }
    if !selected_note.get_refs().is_empty() {
        let mut str_refs = String::new();
        for refer in selected_note.get_refs() {
            str_refs += &(refer + &", ");
        }
        str_refs = str_refs.trim_end_matches(", ").to_string();
        note_description.push(Spans::from(vec![Span::styled("refs:", Style::default())]));
        note_description.push(Spans::from(vec![Span::styled(str_refs, Style::default())]));
    }
    let note_description_paragraph = Paragraph::new(note_description)
        .wrap(Wrap { trim: true })
        .block(notes);
    (list, note_description_paragraph)
}
